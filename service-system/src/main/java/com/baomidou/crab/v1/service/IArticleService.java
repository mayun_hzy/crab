package com.baomidou.crab.v1.service;

import com.baomidou.crab.v1.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文章表 服务类
 * </p>
 *
 * @author jobob
 * @since 2019-02-07
 */
public interface IArticleService extends IService<Article> {

}
